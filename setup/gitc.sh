#!/bin/bash
clear
spinner=( '|' '/' '-' '\' );

ACTION=$1;
TICKET_NO=$2;
FEATURE=$3;

spin(){
    while [ 1 ]
    do
        for i in ${spinner[@]};
        do
            echo -ne "\r$i";
            sleep 0.2;
        done;
    done
}

manual () {
    echo
    echo "CLI for Git to assists in Branching and Rebase"
    echo
    echo "Syntax: gitc [<options> <jira-ticket-number>]"
    echo " options:"
    echo "   f|feature <jira-ticket-number>    Create base branch and `-develop` branch,"
    echo "                                     any development is done on the base branch."
    echo "                                     e.g. gitc/gitc.sh f INSPECTION-1"
    echo
    echo "   x|hotfix <jira-ticket-number>     Create base branch and `-develop` branch,"
    echo "                                     any development is done on the base branch."
    echo "                                     e.g. gitc/gitc.sh x INSPECTION-1"
    echo
    echo "   b|bug <jira-ticket-number>        Create base branch and `-develop` branch,"
    echo "                                     any development is done on the base branch."
    echo "                                     e.g. gitc/gitc.sh x INSPECTION-1"
    echo
    echo "   c|commit <message> <feature: feature | hotfix | bug> <jira-ticket-number>"      
    echo "                                     Commit changes for tickets"
    echo
    echo "   -h|help                           Print this Help."
    echo
}

feature() {
    NO=$1;
    spin &
    pid=$!
    
    for i in `seq 1`
    do
        git checkout master;
        git pull origin;
    done
    for i in `seq 1`
    do
        git checkout -b feature/"$NO";
    done
    for i in `seq 1`
    do
        git checkout -b feature/"$NO"-develop;
    done
    for i in `seq 1`
    do
        git push -u origin feature/"$NO";
        git push -u origin feature/"$NO"-develop;
    done
    for i in `seq 1`
    do
        git checkout feature/"$NO";
        echo
        echo "feature/"$NO" has been created successfully.";
    done
    
    kill $pid  
}

bug() {
    NO=$1;
    spin &
    pid=$!
    
    for i in `seq 1`
    do
        git checkout master;
        git pull origin;
    done
    for i in `seq 1`
    do
        git checkout -b bug/"$NO";
    done
    for i in `seq 1`
    do
        git checkout -b bug/"$NO"-develop;
    done
    for i in `seq 1`
    do
        git push -u origin bug/"$NO";
        git push -u origin bug/"$NO"-develop;
    done
    for i in `seq 1`
    do
        git checkout bug/"$NO";
        echo
        echo "bug/"$NO" has been created successfully.";
    done
    
    kill $pid  
}

hotfix() {
    NO=$1;
    spin &
    pid=$!
    
    for i in `seq 1`
    do
        git checkout master;
        git pull origin;
    done
    for i in `seq 1`
    do
        git checkout -b hotfix/"$NO";
    done
    for i in `seq 1`
    do
        git checkout -b hotfix/"$NO"-develop;
    done
    for i in `seq 1`
    do
        git push -u origin hotfix/"$NO";
        git push -u origin hotfix/"$NO"-develop;
    done
    for i in `seq 1`
    do
        git checkout hotfix/"$NO";
        echo
        echo "hotfix/"$NO" has been created successfully.";
    done
    
    kill $pid  
}

commit() {
    # MSG=$3;
    F=$2;
    NO=$1;

    git add .;
    git commit -m "$MSG";
    git push -u origin "$F/$NO";
    git checkout "$F/$NO-develop"
    git cherry-pick "$F/$NO";
    git cherry-pick --continue;
    git push -u origin "$F/$NO"-develop;
}

docommit() {
    F=$2;
    NO=$1;
    spin &
    pid=$!

    echo "Wait while we fetch a few things:"
    echo -n "Please Input Commit Message: "
    read message

    for i in `seq 1`
    do
    #Git Command
        git add .;
        git commit -m "$message";
    done

    echo "Successfuly Commited the "$F/$NO;
    echo -n "Do you want to clone & push to origin ? [Y/n]: ";
    read answer

    if [ "$answer" == 'Y' ] || [ "$answer" == 'y' ] ; then
        for i in `seq 1`
        do
            git push -u origin "$F/$NO";
            echo "Pushed to Origin "$F/$NO;
        done
        for i in `seq 1`
        do
            git checkout "$F/$NO-develop";
            git cherry-pick "$F/$NO";
            git cherry-pick --continue;
            echo "Done Merging "$F/$NO"-develop";
        done
        for i in `seq 1`
        do
            git push -u origin "$F/$NO"-develop;
            echo "Pushed to Origin "$F/$NO"-develop";
            git checkout "$F/$NO";
            echo "Success";
        done
    elif [ "$answer"  == 'n' ] ; then 
        echo "Done.";
    fi
}

if [ "$ACTION" == '-h' ] || [ "$ACTION" == '-help' ] ; then
    manual
elif [ "$ACTION" == '-f' ] || [ "$ACTION" == 'f' ] || [ "$ACTION" == '-feature' ] ; then
    feature "$TICKET_NO"
elif [ "$ACTION" == '-b' ] || [ "$ACTION" == 'b' ] || [ "$ACTION" == '-bug' ] ; then
    bug
elif [ "$ACTION" == '-x' ] || [ "$ACTION" == 'x' ] || [ "$ACTION" == '-hotfix' ] ; then
    hotfix
elif [ "$ACTION" == '-c' ] || [ "$ACTION" == 'c' ] || [ "$ACTION" == '-commit' ]; then
    # commit "$MESSAGE" "$FEATURE" "$TICKET_NO";
    docommit $FEATURE $TICKET_NO
else
    manual
fi
