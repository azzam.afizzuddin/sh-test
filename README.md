# Git Script Helper

## Introduction
This sh script is to serve as git helper in helping engineer's to create a systematic branch.

## Rag CLI
This boilerplate will use `setup/gitc.sh` to scaffold some common tasks. 

Accepted commands:

| Command       | Alias       | Remarks                                          |
|---------------|-------------|--------------------------------------------------|
| feature       | f           |create a feature branch feature/`<jira-ticket-no>`|
| hotfix        | x           |create a hotfix branch hotfix/`<jira-ticket-no>`  |
| bug           | b           |create a bug branch bug/`<jira-ticket-no>``       |
| commit        | c           |commit changes during dev [have options to clone] |
| help          | -h          |options to view list of helpers                   |

To begin development, you can use this commands:

### For Windows

```
$ setup\gitc.sh -h 
```
**Notice on Windows**

You can use `git-bash` as your default terminal. In fact, when you execute this command, it will 
open up git-bash (if it is installed).

Open up `git-bash`, change directory to your working project, and execute the above command.

### For MacOS & Linux

```
$ setup/gitc.sh -h 
```

Each of the functions may varies and you can run `setup\gitc.sh -h` to see each of the available options.